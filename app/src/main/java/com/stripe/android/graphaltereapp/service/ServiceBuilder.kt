package com.stripe.android.graphaltereapp.service

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ServiceBuilder {
    private val client = OkHttpClient.Builder().build()
    // 8080/graphaltere-0.0.1-SNAPSHOT/
    private val retrofit = Retrofit.Builder()
        .baseUrl("https://graphalterebsg.osc-fr1.scalingo.io/")
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build()

    fun<T> buildService(service: Class<T>): T{
        return retrofit.create(service)
    }
}