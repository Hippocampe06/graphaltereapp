package com.stripe.android.graphaltereapp.model

class Equipement {

    private var id: Number? = null
    private var nom: String? = null

    private var state: State? = null

    override fun toString(): String {
        return "${nom}"
    }

    fun getId(): Number? {
        return id
    }

    fun setNom(nom: String) {
        this.nom = nom
    }

    fun setId(id: Number) {
        this.id = id
    }

    fun getStateName(): String? {
        return state!!.getName()
    }

}