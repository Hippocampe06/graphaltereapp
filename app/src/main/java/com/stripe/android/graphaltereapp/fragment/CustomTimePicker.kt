package com.stripe.android.graphaltereapp.fragment

import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.google.gson.Gson
import com.google.gson.internal.LinkedTreeMap
import com.stripe.android.graphaltereapp.ApiRetrofit
import com.stripe.android.graphaltereapp.R
import com.stripe.android.graphaltereapp.model.Equipement
import com.stripe.android.graphaltereapp.service.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.random.Random

class CustomTimePicker: DialogFragment() {

    private var equipement: Equipement? = null
    private var layout: LinearLayout? = null
    private var creneau: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_custom_time_picker, container, false)
        layout = view.findViewById(R.id.lin_layout)
        return view
    }

    override fun onStart() {
        super.onStart()
        val width = (resources.displayMetrics.widthPixels * 0.70).toInt()

        val c = Calendar.getInstance()
        var hour = c.get(Calendar.HOUR_OF_DAY)
        var minute = c.get(Calendar.MINUTE)

        var isReservable = true

        if (hour > 17) {
            isReservable = false
        }

        var isResa: Boolean = false

        //if (equipement!!.getStateName() == "USED" || equipement!!.getStateName() == "OFF") {
            isResa = true
        //} else {
        //    isResa = true
        //}

        // Appel API pour avoir un tableau de temps déjà réservés

        // Si heure < 17h
        if (isReservable) {

            var tempHour = hour
            if (hour < 8) {
                tempHour = 8
            }

            for (i in tempHour..17) {

                var creneaux: MutableList<String>? = null

                if (i == hour && minute > 30) {
                    creneaux = mutableListOf("$i : 30")
                } else {
                    creneaux = mutableListOf("$i : 00", "$i : 30")
                }


                for (c in creneaux) {

                    val rd = Random.nextInt(0, 10)

                    val linLayout = LinearLayout(context)
                    linLayout.gravity = Gravity.CENTER
                    linLayout.orientation = LinearLayout.VERTICAL
                    linLayout.setPadding(20, 20, 20, 20)

                    val textView = TextView(context)
                    textView.textSize = 22.0f
                    if (rd > 5) {
                        textView.setTextColor(resources.getColor(R.color.reservable))
                    } else {
                        textView.setTextColor(resources.getColor(R.color.non_reservable))
                    }

                    textView.text = c

                    textView.setOnClickListener {
                        this.creneau = c
                        if (rd > 5) {
                            Toast.makeText(context, "Le créneau : ${this.creneau} a été réservé !", Toast.LENGTH_SHORT).show()
                            if (reserverCreneau()) {
                                dialog!!.dismiss()
                            }

                        } else {
                            Toast.makeText(context, "Le créneau : ${this.creneau} ne peut pas être réservé !", Toast.LENGTH_SHORT).show()
                        }

                    }
                    linLayout.addView(textView)
                    layout!!.addView(linLayout)
                }
            }

        } else {
            val linLayout = LinearLayout(context)
            linLayout.gravity = Gravity.CENTER
            linLayout.orientation = LinearLayout.VERTICAL
            linLayout.setPadding(20, 20, 20, 20)

            val text = TextView(context)
            text.text = "Aucun horaire réservable pour aujourd'hui..."
            linLayout.addView(text)
            layout!!.addView(linLayout)
        }


        dialog!!.window?.setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    fun show(manager: FragmentManager, tag: String?, equipement: Equipement) {
        super.show(manager, tag)
        this.equipement = equipement
    }

    private fun reserverCreneau(): Boolean {
        /*var ret: Boolean? = null

        val request = ServiceBuilder.buildService(ApiRetrofit::class.java)
        val call = request.resaEquipement(1, 1)

        call.enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                if (response.isSuccessful && response.body() != null){

                    val res = response.body()

                    Log.d("resApiCall", res.toString())

                    ret = true


                } else {
                    Log.d("responseNotSuccessful :", "pas woop woop = ${response.toString()}")
                    ret = false
                }
            }
            override fun onFailure(call: Call<Any>, t: Throwable) {
                Log.d("responseNotSuccessful :", "pas du tout woop woop ${t.toString()}")
                ret = false
            }
        })

        return ret!!*/

        return true

    }


}