package com.stripe.android.graphaltereapp.activity

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import com.stripe.android.graphaltereapp.R

class SplashActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        // TODO

        val ctx = this

        val progressBar = findViewById<ProgressBar>(R.id.progress_bar)

        val animator: ValueAnimator = ValueAnimator.ofInt(0, progressBar.max)
        animator.duration = 1000
        animator.addUpdateListener { animation -> progressBar.progress = animation.animatedValue as Int }
        animator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                super.onAnimationEnd(animation)

                if (baseContext.getSharedPreferences(getString(R.string.access_token), Context.MODE_PRIVATE).getString("access_token", "") == "") {
                    val intent = Intent(ctx, LoginActivity::class.java)
                    startActivity(intent)
                } else {
                    val intent = Intent(ctx, MainActivity::class.java)
                    startActivity(intent)
                }

            }
        })
        animator.start()

    }

}