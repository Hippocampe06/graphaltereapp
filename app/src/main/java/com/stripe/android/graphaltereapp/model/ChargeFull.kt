package com.stripe.android.graphaltereapp.model

data class ChargeFull(var card: Card, var charge: Charge, var token: String) {
}