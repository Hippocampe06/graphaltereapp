package com.stripe.android.graphaltereapp.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.github.aachartmodel.aainfographics.aachartcreator.AAChartModel
import com.github.aachartmodel.aainfographics.aachartcreator.AAChartType
import com.github.aachartmodel.aainfographics.aachartcreator.AAChartView
import com.github.aachartmodel.aainfographics.aachartcreator.AASeriesElement
import com.github.florent37.expansionpanel.ExpansionHeader
import com.github.florent37.expansionpanel.ExpansionLayout
import com.github.nkzawa.socketio.client.Socket
import com.google.android.material.navigation.NavigationView
import com.google.gson.Gson
import com.google.gson.internal.LinkedTreeMap
import com.stripe.android.graphaltereapp.ApiRetrofit
import com.stripe.android.graphaltereapp.R
import com.stripe.android.graphaltereapp.fragment.CustomTimePicker
import com.stripe.android.graphaltereapp.model.Equipement
import com.stripe.android.graphaltereapp.service.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.random.Random


class MainActivity: AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private var drawerLayout: DrawerLayout? = null
    private var navigationView: NavigationView? = null
    private var mSocket: Socket? = null

    // API related
    private var accessToken: String? = null
    private var authority: String? = null

    // Models for charts
    private var aaChartModelRevenus: AAChartModel? = null
    private var aaChartViewRevenus: AAChartView? = null
    private var aaChartModelPopulation: AAChartModel? = null
    private var aaChartViewPopulation: AAChartView? = null
    private var aaChartModelAge: AAChartModel? = null
    private var aaChartViewAge: AAChartView? = null

    private var equipementObjects: MutableList<Equipement>? = mutableListOf()

    var semiGray: Int? = null
    var fadedBlue: Int? = null
    var lightRed: Int? = null


    // SeriesElements
    var dataOfAaSeriesElementRevenus: MutableList<Any> = mutableListOf(9.0, 8.0, 7.5, 9.0, 10.0, 10.0, 9.5, 9.0, 6.0, 3.0, 0.0, 0.0)
    var aaSeriesElementRevenus = arrayOf(AASeriesElement()
            .name("Machine")
            .data(dataOfAaSeriesElementRevenus.toTypedArray()))
    var dataOfAaSeriesElementPopulation: MutableList<Any> = mutableListOf(26.0, 23.0, 21.0, 25.0, 26.0, 26.0, 24.0, 20.0, 15.0, 7.0, 0.0, 0.0)
    var aaSeriesElementPopulation = arrayOf(AASeriesElement()
            .name("Population")
            .data(dataOfAaSeriesElementPopulation.toTypedArray()))



    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        drawerLayout = findViewById(R.id.drawer_layout)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        accessToken = baseContext.getSharedPreferences(getString(R.string.access_token), Context.MODE_PRIVATE).getString("access_token", "")
        authority = baseContext.getSharedPreferences(getString(R.string.authority), Context.MODE_PRIVATE).getString("authority", "")

        Log.d("authority", authority!!)

        if (authority == "ROLE_USER") {
            findViewById<ExpansionHeader>(R.id.expansionheader2).visibility = View.GONE
            findViewById<ExpansionLayout>(R.id.expansionLayout2).visibility = View.GONE
            findViewById<ExpansionHeader>(R.id.expansionheader3).visibility = View.GONE
            findViewById<ExpansionLayout>(R.id.expansionLayout3).visibility = View.GONE
        }

        semiGray = resources.getColor(R.color.semitransparentGray, application.theme)
        fadedBlue = resources.getColor(R.color.fadedBlue, application.theme)
        lightRed = resources.getColor(R.color.lightRed, application.theme)


        // ModelRevenus
        aaChartModelRevenus = AAChartModel()
                .chartType(AAChartType.Waterfall)
                .title("Revenus cumulés")
                .backgroundColor("#efefef")
                .yAxisTitle("€")
                .dataLabelsEnabled(false)
                .series(aaSeriesElementRevenus)

        aaChartViewRevenus = findViewById(R.id.aa_chart_view_revenus)
        aaChartViewRevenus!!.aa_drawChartWithChartModel(aaChartModelRevenus!!)


        // ModelPopulation
        aaChartModelPopulation = AAChartModel()
                .chartType(AAChartType.Spline)
                .title("Population")
                .backgroundColor("#efefef")
                .yAxisTitle("Nombre personnes")
                .dataLabelsEnabled(false)
                .series(aaSeriesElementPopulation)

        aaChartViewPopulation = findViewById(R.id.aa_chart_view_population)
        aaChartViewPopulation!!.aa_drawChartWithChartModel(aaChartModelPopulation!!)


        // ModelAge
        aaChartModelAge = AAChartModel()
                .chartType(AAChartType.Pie)
                .title("Répartition âge")
                .backgroundColor("#efefef")
                .dataLabelsEnabled(false)
                .series(arrayOf(
                        AASeriesElement()
                                .name("Pourcentage")
                                .data(arrayOf(
                                        arrayOf("15-18", 26),
                                        arrayOf("18-25", 48),
                                        arrayOf("25-50", 24),
                                        arrayOf("50-75", 2),
                                ))))


        aaChartViewAge = findViewById(R.id.aa_chart_view_age)
        aaChartViewAge!!.aa_drawChartWithChartModel(aaChartModelAge!!)



        synchronizeSocket()


        val redirect = findViewById<Button>(R.id.redirect_login)
        redirect.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

        getEquipements()
        getEffectifUsers()

        val launch = findViewById<Button>(R.id.launch_data)
        launch.setOnClickListener {
            addDataAndRefreshCharts(14.0)
        }

    }

    fun synchronizeSocket() {
        //Socket instance
        /*val app: SocketInstance = application as SocketInstance
        mSocket = app.getMSocket()
        mSocket?.connect()
        val options = IO.Options()
        options.reconnection = true
        options.forceNew = true

        Log.d("SOCKET", mSocket.toString())


        if(mSocket!!.connected()) {
            Log.d("Socket : " ,"Socket is connected")
        } else {
            Log.d("Socket : " ,"Socket is NOT connected")
        }*/
    }

    fun getEffectifUsers() {
        val request = ServiceBuilder.buildService(ApiRetrofit::class.java)
        val call = request.getNbUsers(accessToken!!)
        Log.d("woopwoop", accessToken!!)

        call.enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                if (response.isSuccessful && response.body() != null) {
                    Log.d("response::::", response.toString())
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                Toast.makeText(this@MainActivity, "${t.message}", Toast.LENGTH_SHORT).show()
                Log.d("responseNotSuccessful :", "pas du tout woop woop ${t.message}")
            }
        })
    }

    fun getEquipements() {

        val request = ServiceBuilder.buildService(ApiRetrofit::class.java)
        val call = request.getEquipements(accessToken!!)
        Log.d("woopwoop", accessToken!!)

        call.enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                if (response.isSuccessful && response.body() != null) {

                    Toast.makeText(this@MainActivity, "${response.body()}", Toast.LENGTH_SHORT).show()

                    val equipements = response.body() as ArrayList<LinkedTreeMap<String, String>>

                    Log.d("equipements", response.body().toString())

                    equipements.forEach {

                        val gson = Gson()
                        val equ = gson.toJson(it)
                        val equipement: Equipement = gson.fromJson(equ, Equipement::class.java)
                        Log.d("equipement", equipement.toString())

                        equipementObjects!!.add(equipement)

                    }

                    Log.d("equipementObjects", equipementObjects.toString())
                    setupUpEquipementIcons()

                } else {
                    Log.d("responseNotSuccessful :", "pas woop woop = ${response.toString()}")
                    val equipem = Equipement()
                    equipem.setNom("Machine n°1")
                    equipem.setId(1)
                    equipementObjects!!.add(equipem)
                    setupUpEquipementIcons()
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                Toast.makeText(this@MainActivity, "${t.message}", Toast.LENGTH_SHORT).show()
                Log.d("responseNotSuccessful :", "pas du tout woop woop ${t.message}")

                val equipem = Equipement()
                equipem.setNom("Machine n°1")
                equipem.setId(1)
                equipementObjects!!.add(equipem)
                setupUpEquipementIcons()
            }
        })

    }

    private fun addDataAndRefreshCharts(elem: Any) {

        val rd1 = Random.nextDouble(0.0, 10.0)
        val rd2 = Random.nextDouble(10.0, 30.0)

        // Update chart population
        dataOfAaSeriesElementPopulation.removeAt(0)
        dataOfAaSeriesElementPopulation.add(rd2)
        aaChartModelPopulation!!.series(aaSeriesElementPopulation)
        aaSeriesElementPopulation[0].data(dataOfAaSeriesElementPopulation.toTypedArray())
        aaChartViewPopulation!!.aa_onlyRefreshTheChartDataWithChartOptionsSeriesArray(aaSeriesElementPopulation, false)


        // Update chart revenus
        dataOfAaSeriesElementRevenus.removeAt(0)
        dataOfAaSeriesElementRevenus.add(rd1)
        aaChartModelRevenus!!.series(aaSeriesElementRevenus)
        aaSeriesElementRevenus[0].data(dataOfAaSeriesElementRevenus.toTypedArray())
        aaChartViewRevenus!!.aa_onlyRefreshTheChartDataWithChartOptionsSeriesArray(aaSeriesElementRevenus, false)

        // Le chart age ne s'update pas pour le moment

    }

    private fun setupUpEquipementIcons() {

        val layout = findViewById<LinearLayout>(R.id.layout_equipements)

        equipementObjects!!.forEach {

            val linLayout = LinearLayout(this)
            linLayout.gravity = Gravity.CENTER
            linLayout.orientation = LinearLayout.VERTICAL

            linLayout.setPadding(20, 20, 20, 20)

            /*if (it.getStateName() == "USED") {
                linLayout.setBackgroundColor(lightRed!!)
            } else {*/
                linLayout.setBackgroundColor(semiGray!!)
            //}

            val layoutParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(400, LinearLayout.LayoutParams.MATCH_PARENT)
            layoutParams.setMargins(10, 10, 10, 10)

            val imageView = ImageView(this)
            imageView.setImageDrawable(resources.getDrawable(R.drawable.sports_logo, application.theme))

            val dynamicTextview = TextView(this)
            dynamicTextview.maxWidth = 300
            dynamicTextview.gravity = Gravity.CENTER
            dynamicTextview.setPadding(5, 5, 5, 5)

            linLayout.addView(imageView)
            linLayout.addView(dynamicTextview)

            val equipement = it

            linLayout.setOnClickListener {
                //showTimePickerDialog(equipement)
                showCustomTimePickerDialog(equipement)
            }

            dynamicTextview.text = it.toString()
            layout.addView(linLayout, layoutParams)

        }

    }

    private fun showTimePickerDialog(equipement: Equipement) {
        //TimePickerFragment().show(supportFragmentManager, "timePicker", equipement.getId())
    }
    private fun showCustomTimePickerDialog(equipement: Equipement) {
        CustomTimePicker().show(supportFragmentManager, "MyCustomFragment", equipement)
    }


    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.resigned -> {
                Toast.makeText(baseContext, "Se désabonner", Toast.LENGTH_SHORT).show()
            }
            R.id.rate -> {
                Toast.makeText(baseContext, "Noter l'application", Toast.LENGTH_SHORT).show()
            }
            R.id.disconnect -> {
                Log.d("userdisconnect", "user tried to disconnect")
            }
        }
        drawerLayout!!.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        super.onOptionsItemSelected(item)
        when (item.itemId) {
            R.id.resigned -> {
                Toast.makeText(baseContext, "TODO : QUAND PAIEMENT RÉCURRENT, IMPLÉMENTER LE DÉSABONNEMENT", Toast.LENGTH_SHORT).show()
            }
            R.id.rate -> {
                Toast.makeText(baseContext, "Noter l'application", Toast.LENGTH_SHORT).show()
            }
            R.id.disconnect -> {
                val sharedPref = baseContext.getSharedPreferences(getString(R.string.access_token), Context.MODE_PRIVATE)
                with (sharedPref.edit()) {
                    putString(getString(R.string.access_token), "")
                    putString(getString(R.string.authority), "")
                    apply()
                }
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
            }
        }
        drawerLayout!!.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        if (drawerLayout!!.isDrawerOpen(GravityCompat.START)) {
            drawerLayout!!.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.drawer_view, menu)
        return true
    }



    override fun onDestroy() {
        super.onDestroy()
        //mSocket!!.disconnect()
    }

}