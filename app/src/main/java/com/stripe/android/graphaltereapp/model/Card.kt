package com.stripe.android.graphaltereapp.model

data class Card(var number: String, var month: String, var year: String, var cvv: String) {
}