package com.stripe.android.graphaltereapp.model

class State {

    private var id: Number? = null
    private var name: String? = null

    fun getId(): Number? {
        return id
    }
    fun getName(): String? {
        return name
    }

}