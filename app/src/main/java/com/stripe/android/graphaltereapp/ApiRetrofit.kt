package com.stripe.android.graphaltereapp

import com.stripe.android.graphaltereapp.model.ChargeFull
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiRetrofit {



    // Users
    @Headers("Content-Type: application/json")
    @POST("/users/")
    fun getUser(@Body() requestBody: RequestBody): Call<Any>

    @Headers("Content-Type: application/json")
    @POST("/api/auth/signup")
    fun createUser(@Body() requestBody: RequestBody): Call<Any>

    @Headers("Content-Type: application/json")
    @GET("/users/effective/")
    fun getNbUsers(@Header("Authorization") authHeader: String): Call<Any>
    // /users/effective


    // Payment
    @Headers("Content-Type: application/json")
    @POST("/payements/payement")
    fun createPayment(@Body() requestBody: RequestBody): Call<Any>


    // Equipements
    @Headers("Content-Type: application/json")
    @GET("/equipements/")
    fun getEquipements(@Header("Authorization") authHeader: String): Call<Any>

    @Headers("Content-Type: application/json")
    @POST("/equipements/update/{id}")
    fun updateEquipementById(@Path("id") @Body() requestBody: RequestBody): Call<Any>

    @Headers("Content-Type: application/json")
    @POST("/users/{idUser}/reservation/{idEquipement}/equipement")
    fun resaEquipement(@Path("idUser") idUser: Number, @Path("idEquipement") idEquipement: Number, @Body() requestBody: RequestBody): Call<Any>



    // Auth
    @Headers("Content-Type: application/json")
    @POST("/api/auth/signin")
    fun login(@Body() requestBody: RequestBody): Call<Any>

    @Headers("Content-Type: application/json")
    @POST("/api/auth/signup")
    fun createAccount(@Body() requestBody: RequestBody): Call<Any>

}