package com.stripe.android.graphaltereapp.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatEditText
import com.stripe.android.graphaltereapp.ApiRetrofit
import com.stripe.android.graphaltereapp.R
import com.stripe.android.graphaltereapp.model.Card
import com.stripe.android.graphaltereapp.model.Charge
import com.stripe.android.graphaltereapp.model.ChargeFull
import com.stripe.android.graphaltereapp.service.ServiceBuilder
import com.vinaygaba.creditcardview.CreditCardView
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class StripeActivity: AppCompatActivity() {

    var checkOut: Button? = null
    var addPayment: Button? = null

    private var number: String? = null
    private var month: String? = null
    private var year: String? = null
    private var cvv: String? = null

    private var token: String? = null
    private var email: String? = null

    var creditCardView: CreditCardView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stripe)

        // Récupération du secretId retourné par l'API identifiant la transaction Stripe
        try {
            token = intent.getStringExtra("secretid")!!
            email = intent.getStringExtra("emailStripe")!!
            Log.d("HEHEHEHE", token!!)
        } catch (e: Exception) {
            token = "Erreur de secretId"
            Log.d("AAAAAAA", "Erreur de secretId")
        }


        creditCardView = findViewById(R.id.card_view)
        //addPayment = findViewById(R.id.add_payment)
        checkOut = findViewById(R.id.pay_button)

        checkOut!!.setOnClickListener {
            // Si pas de data vide, on appelle l'API
            if (getDataFromForm()) {
                callApiCreateUser()
            } else {
                Toast.makeText(this@StripeActivity, "Il manque de la donnée mon ami !!!", Toast.LENGTH_SHORT).show()
            }
        }

    }


    private fun callApiCreateUser() {

        val card = JSONObject()
        card.put("number", number)
        card.put("cvv", cvv)
        card.put("month", month)
        card.put("year", year)


        val charge = JSONObject()
        charge.put("description", "Abonnement")
        charge.put("amount", 2500)
        charge.put("currency", "EUR")
        charge.put("stripeEmail", email)
        charge.put("stripeToken", token!!)

        val jsonObject = JSONObject()
        jsonObject.put("card", card)
        jsonObject.put("charge", charge)
        jsonObject.put("token", token!!)

        val jsonObjectString = jsonObject.toString()
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        Log.d("requestBody", requestBody.toString())
        Log.d("jsonObject", jsonObject.toString())

        val request = ServiceBuilder.buildService(ApiRetrofit::class.java)
        val call = request.createPayment(requestBody)

        call.enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                if (response.isSuccessful){

                    Log.d("RESPONSE : ", response.body().toString())

                    Toast.makeText(this@StripeActivity, "${response.body().toString()}", Toast.LENGTH_SHORT).show()

                    val intent = Intent(this@StripeActivity, LoginActivity::class.java)
                    startActivity(intent)


                } else {
                    Log.d("responseNotSuccessful :", response.toString())
                }
            }
            override fun onFailure(call: Call<Any>, t: Throwable) {
                Toast.makeText(this@StripeActivity, "${t.message}", Toast.LENGTH_SHORT).show()
                Log.d("responseNotSuccessful :", "pas du tout woop woop ${t.message}")
            }
        })
    }

    private fun getDataFromForm(): Boolean {

        number = creditCardView!!.cardNumber
        month = creditCardView!!.expiryDate.split('/')[0]
        year = creditCardView!!.expiryDate.split('/')[1]
        cvv = findViewById<EditText>(R.id.cvv).text.toString()

        return (number != "" && month != "" && year != "" && cvv != "")

    }

}