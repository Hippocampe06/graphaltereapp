package com.stripe.android.graphaltereapp.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatEditText
import com.stripe.android.graphaltereapp.ApiRetrofit
import com.stripe.android.graphaltereapp.R
import com.stripe.android.graphaltereapp.service.ServiceBuilder
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CreateAccountActivity: AppCompatActivity() {


    private var login: String? = null
    private var nom: String? = null
    private var prenom: String? = null
    private var password: String? = null
    private var email: String? = null

    private var secretId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_account)

        val redirect = findViewById<TextView>(R.id.redirect_login)
        redirect.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

        val create = findViewById<Button>(R.id.create_button)
        create.setOnClickListener {
            if (getDataFromForm()) {
                callApiCreateUser()
            } else {
                Toast.makeText(this@CreateAccountActivity, "Il manque de la donnée mon ami !!!", Toast.LENGTH_SHORT).show()
            }

        }

    }

    private fun callApiCreateUser() {

        val jsonObject = JSONObject()
        jsonObject.put("nom", nom)
        jsonObject.put("prenom", prenom)
        jsonObject.put("login", login)
        jsonObject.put("email", email)
        jsonObject.put("password", password)



        val jsonObjectString = jsonObject.toString()
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        val request = ServiceBuilder.buildService(ApiRetrofit::class.java)
        val call = request.createUser(requestBody)

        call.enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                if (response.isSuccessful){

                    secretId = response.body().toString().split("!")[1].split(" , ")[0]

                    Log.d("SECRETID", secretId!!)

                    Toast.makeText(this@CreateAccountActivity, "${secretId!!}", Toast.LENGTH_SHORT).show()

                    val intent = Intent(this@CreateAccountActivity, StripeActivity::class.java)
                    intent.putExtra("secretid", secretId)
                    intent.putExtra("emailStripe", email)
                    startActivity(intent)


                } else {
                    Log.d("responseNot :", response.toString())
                }
            }
            override fun onFailure(call: Call<Any>, t: Throwable) {
                Toast.makeText(this@CreateAccountActivity, "${t.message}", Toast.LENGTH_SHORT).show()
                Log.d("responseNotSuccessful :", "pas du tout woop woop ${t.message}")
            }
        })
    }

    private fun getDataFromForm(): Boolean {

        nom = findViewById<AppCompatEditText>(R.id.surname).text.toString()
        prenom = findViewById<AppCompatEditText>(R.id.name).text.toString()
        email = findViewById<AppCompatEditText>(R.id.email).text.toString()
        password = findViewById<AppCompatEditText>(R.id.password).text.toString()
        login = findViewById<AppCompatEditText>(R.id.login).text.toString()


        return (nom != "" && prenom != "" && email != "" && password != "" && login != "")

    }


}