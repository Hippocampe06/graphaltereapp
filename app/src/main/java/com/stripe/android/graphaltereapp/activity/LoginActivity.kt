package com.stripe.android.graphaltereapp.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatEditText
import com.stripe.android.graphaltereapp.ApiRetrofit
import com.stripe.android.graphaltereapp.R
import com.stripe.android.graphaltereapp.service.ServiceBuilder
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity: AppCompatActivity() {

    private var login: String? = null
    private var password: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val redirect = findViewById<TextView>(R.id.redirect_create_account)


        redirect.setOnClickListener {
            val intent = Intent(this, CreateAccountActivity::class.java)
            startActivity(intent)
        }

        val redirectConnect = findViewById<Button>(R.id.login_button)

        redirectConnect.setOnClickListener {
            //getUser
            // Si pas de data vide, on appelle l'API
            if (getDataFromForm()) {
                callApiGetUser()
            } else {
                Toast.makeText(this@LoginActivity, "Il manque de la donnée mon ami !!!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun callApiGetUser() {

        val jsonObject = JSONObject()
        jsonObject.put("username", login)
        jsonObject.put("password", password)


        val jsonObjectString = jsonObject.toString()
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())

        val request = ServiceBuilder.buildService(ApiRetrofit::class.java)
        val call = request.login(requestBody)

        call.enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                if (response.isSuccessful){

                    val accessToken = response.body().toString().split("accessToken=")[1].split(", tokenType=")[0]
                    val authority = response.body().toString().split("[{authority=")[1].split("}]")[0]
                    Log.d("aaaaaaaaa", accessToken)
                    Log.d("response.body()", response.body().toString())

                    val sharedPref = baseContext.getSharedPreferences(getString(R.string.access_token), Context.MODE_PRIVATE)
                    with (sharedPref.edit()) {
                        putString(getString(R.string.access_token), "Bearer ${accessToken}")
                        putString(getString(R.string.authority), authority)
                        apply()
                    }

                    val intent = Intent(this@LoginActivity, MainActivity::class.java)
                    startActivity(intent)

                } else {
                    Log.d("responseNotSuccessful :", response.toString())
                    Toast.makeText(this@LoginActivity, "Réessayez", Toast.LENGTH_SHORT).show()
                }
            }
            override fun onFailure(call: Call<Any>, t: Throwable) {
                Toast.makeText(this@LoginActivity, "${t.message}", Toast.LENGTH_SHORT).show()
                Log.d("responseNotSuccessful :", "pas du tout woop woop ${t.message}")
            }
        })

    }

    fun getDataFromForm(): Boolean {
        login = findViewById<AppCompatEditText>(R.id.login).text.toString()
        password = findViewById<AppCompatEditText>(R.id.password).text.toString()

        return (login != "" && password != "")
    }
}